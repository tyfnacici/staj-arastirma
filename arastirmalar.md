Son güncelleme tarihi 28/12/2022 Saat 15:26

#  Cloud Computing temel kavramlar araştırması

- Airframe: 

Kuruluşların özel bulut hizmetlerini planlamasına veya değerlendirmesine yardımcı olan açık kaynaklı bir bulut bilişim platformudur.

------------------------

- Anything-as-a-Service (XaaS): 

Abonelik temelinde internet üzerinden ürün veya hizmet sunma modelini tanımlamak için kullanılan bir terimdir. XaaS, bulut tabanlı ürün veya hizmetlerin sunulmasını ifade ettiği bulut bilişim bağlamında yaygın olarak kullanılır.

XaaS, geleneksel BT çözümlerine kıyasla artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların kendi altyapılarına yatırım yapmaya ve sürdürmeye gerek kalmadan ihtiyaç duydukları ürün ve hizmetlere kolayca ve hızlı bir şekilde erişmelerini ve kullanmalarını sağlar.

Genel olarak, XaaS, internet üzerinden ürün ve hizmet sunmak için popüler ve yaygın olarak kullanılan bir modeldir ve kuruluşlara buluttan işletmeleri için etkin bir şekilde yararlanmaları için ihtiyaç duydukları araçları ve kaynakları sağlar.

-----------------------------

- Desktop-as-a-service: 

Kuruluşların kullanıcılarına sanal bir masaüstü ortamına erişim sağlamaları için popüler ve etkili bir yoldur ve herhangi bir cihazdan, herhangi bir yerden çalışmalarına ve işbirliği yapmalarına olanak tanır.

--------------------------

- Infrastructure-as-a-Service: 

internet üzerinden bilgi işlem altyapısına erişim sağlayan bir tür bulut bilişim hizmetidir. IaaS, kuruluşların uygulamalarını, verilerini ve diğer işletme varlıklarını bulutta kolayca ve hızlı bir şekilde dağıtmasına ve yönetmesine olanak tanır.

IaaS tipik olarak sanallaştırma, depolama, ağ oluşturma ve güvenlik dahil olmak üzere bir dizi farklı hizmet ve araç içerir. Bu hizmetler ve araçlar, kuruluşların kendi altyapılarını yönetmek ve sürdürmek yerine ana işlerine odaklanmalarına olanak tanıyan IaaS sağlayıcısı tarafından yönetilir ve sürdürülür.

IaaS, artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve geleneksel şirket içi altyapıya kıyasla daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak bulut kaynaklarına her yerden kolayca erişmelerini ve yönetmelerini sağlar.

--------------

- Software as a Service: 

SaaS, uygulamaları sağlayan ve bunları çevrimiçi olarak müşteriye sunan bir yazılım dağıtım modelidir.

------------------

- Cloud App: 

Bulut uygulaması, bulut bilişim teknolojisi kullanılarak internet üzerinden teslim edilen ve erişilen bir yazılım uygulamasıdır. Bir bulut uygulaması genellikle Amazon Web Hizmetleri (AWS), Microsoft Azure veya Google Cloud Platform gibi bir bulut bilişim platformunda çalışır ve kullanıcılar tarafından bir web tarayıcısı veya başka bir istemci uygulaması aracılığıyla erişilir.

--------------

- Cloud Application Management for Platforms (CAMP): 

Bulut tabanlı uygulamaları yönetmenin standart bir yolunu tanımlar. CAMP, bulut uygulamalarının herhangi bir bulut bilişim platformunda dağıtılmasına ve yönetilmesine olanak tanıyan platformdan bağımsız bir şekilde yönetilmesi gerektiği fikrine dayanmaktadır.

CAMP, bulut uygulamaları tarafından temel bulut platformlarıyla iletişim kurmak için kullanılabilecek bir dizi API ve diğer arabirimleri tanımlar. Bu API'ler ve arayüzler, platforma özel araçlara veya bilgiye ihtiyaç duymadan uygulamaların farklı bulut platformlarında kolayca dağıtılmasına, yönetilmesine ve izlenmesine olanak tanır

--------------

- Cloud Backup:

Bulut yedekleme, verileri uzak, site dışı bir konuma, tipik olarak bir bulut bilişim ortamına kopyalayarak depolamak ve korumak için kullanılan bir yöntemdir. Bulut yedekleme, kuruluşun şirket içi altyapısını etkileyen bir felaket veya başka bir olay durumunda, bir kuruluşun verileri için ek bir koruma katmanı sağlamak üzere tasarlanmıştır.

-------------------

- Cloud Backup Service Provider: 

Bulut yedekleme hizmeti sağlayıcısı, kuruluşlara bulut tabanlı yedekleme ve kurtarma hizmetleri sunan bir şirkettir. Bir bulut yedekleme hizmeti sağlayıcısı, kuruluşların verilerini korumalarına ve yönetmelerine yardımcı olmak için genellikle bulut depolama, yedekleme yazılımı ve kurtarma araçları dahil olmak üzere bir dizi farklı hizmet ve araç sağlar.

-----------

- Cloud Backup Solutions: 

Bulut yedekleme çözümleri, kuruluşların verilerini uzak, site dışı bir konuma, tipik olarak bir bulut bilişim ortamına kopyalayarak korumalarına ve yönetmelerine yardımcı olan ürün ve hizmetlerdir.

---------------

- Cloud Computing:

Bulut bilişim, artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve geleneksel şirket içi hesaplamaya kıyasla daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak bilgi işlem kaynaklarına her yerden kolayca erişmelerini ve yönetmelerini sağlar.

---------------

- Cloud Computing Accounting Software: 

Bulut bilişim muhasebe yazılımı, geleneksel şirket içi muhasebe yazılımlarına kıyasla daha fazla mobilite ve esneklik, daha iyi güvenlik ve uyumluluk ve daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca kuruluşların muhasebe yazılımlarına internet bağlantısı olduğu sürece herhangi bir cihazdan, herhangi bir yerden kolayca erişmelerini ve kullanmalarını sağlar.

--------------

- Cloud Computing Reseller: 

Microsoft Azure veya Google Cloud Platform gibi bir veya daha fazla bulut bilişim sağlayıcısıyla ortaklık kurar ve ürünlerini ve hizmetlerini kendi müşterilerine yeniden satar.

Bulut bilişim satıcıları, müşterilerine farklı bulut bilişim ürünleri ve hizmetleri hakkında uzmanlık ve bilgi birikimi ve müşterilerinin özel ihtiyaçlarına ve gereksinimlerine göre uyarlanmış özelleştirilmiş çözümler sunma yeteneği dahil olmak üzere bir dizi avantaj sağlar.


----------------

- Cloud Database: 

Bulut veritabanı, yerel sunucular veya kişisel bilgisayarlar yerine bir bulut bilişim platformunda barındırılan ve yönetilen bir veritabanıdır. Bir bulut veritabanı genellikle kullanıcılara internet üzerinden veri depolama, yönetim ve analitik gibi bir dizi hizmet ve araca erişim sağlar.


-----------

- Cloud Enablement: 

Bulut etkinleştirme, bulut bilişim hizmetlerinin dağıtımını ve kullanımını desteklemek için bir kuruluşun BT altyapısını ve sistemlerini hazırlama sürecidir. 

-------------

- Cloud Management: 

Bulut yönetimi, bir kuruluş içindeki bulut bilişim hizmetlerinin dağıtımını ve kullanımını denetleme ve yönetme sürecidir. 

-------

- Cloud Migration: 

Bulut geçişi, verileri, uygulamaları ve diğer iş öğelerini bir kuruluşun şirket içi altyapısından bulut bilişim ortamına taşıma sürecini ifade eder. 

--------

- Cloud OS: 

Bulut işletim sistemi (OS), bulut bilişim ortamında kullanılmak üzere tasarlanmış bir işletim sistemi türüdür. Bu tür bir işletim sistemi tipik olarak, hizmet olarak altyapı (IaaS), hizmet olarak platform (PaaS) ve hizmet olarak yazılım (SaaS) gibi bir bulut bilişim platformu tarafından sağlanan çeşitli kaynakları ve hizmetleri yönetmek ve koordine etmek için kullanılır. ).

--------------

- Cloud Portability: 

Bulut taşınabilirliği, bir kuruluşun verilerini, uygulamalarını ve diğer varlıklarını bir bulut bilişim ortamından diğerine taşıma yeteneğini ifade eder.

Bulut taşınabilirliği, esnekliği korumalarına ve satıcı kilitlenmelerinden kaçınmalarına olanak tanıdığı için bulut bilişim kullanan kuruluşlar için önemli bir husustur. Kuruluşlar, buluttan bağımsız teknolojileri ve yaklaşımları kullanarak ve bulut geçiş süreçlerini dikkatli bir şekilde planlayıp yöneterek, gerektiğinde varlıklarının kolayca farklı bir bulut ortamına taşınabilmesini sağlayabilir. 

-------------------

- Cloud Provider: Bulut sağlayıcı, kuruluşlara bulut bilişim hizmetleri sağlayan bir şirkettir.

------------

- Cloud Server Hosting: Bulut sunucu barındırma, genellikle web sitelerini ve web uygulamalarını barındırmak amacıyla sunucuları barındırmak için bulut bilişim teknolojisinin kullanılmasını ifade eder.

-------------

- Cloud Storage: Bulut depolama, verileri depolamak ve yönetmek için genellikle bir bulut bilişim sağlayıcısı tarafından sağlanan uzak sunucuların kullanımını ifade eder. 

----------------

- Enterprise Application: 

Kurumsal uygulama, büyük kuruluşlar tarafından kullanılmak üzere tasarlanmış ve geliştirilmiş bir yazılım uygulamasıdır. Bu uygulamalar genellikle karmaşık ve kapsamlıdır ve çok çeşitli iş işlevlerini ve süreçlerini desteklemek için kullanılır.

----------------

- Enterprise Cloud Backup: 

Kurumsal bulut yedekleme, bir kuruluşun verilerinin yedek kopyalarını depolamak ve yönetmek için bulut bilişim teknolojisinin kullanılmasını ifade eder.

Kurumsal bulut yedekleme, geleneksel yedekleme çözümlerine kıyasla daha fazla güvenilirlik ve kullanılabilirlik, iyileştirilmiş ölçeklenebilirlik ve daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak yedeklenen verilerine her yerden kolayca erişmelerini ve yönetmelerini sağlar.

---------------

- Hybrid Cloud Storage: 

Hibrit bulut depolama, bir kuruluşun verilerini depolamak ve yönetmek için şirket içi depolama ve bulut depolama kombinasyonunun kullanılmasını ifade eder. Bu, kuruluşların hem şirket içi hem de bulut depolamanın avantajlarından yararlanmasına ve farklı veri türleri ve iş yükleri için en uygun depolama çözümünü seçmesine olanak tanır.

----------------

- Internal(PRIVATE) Cloud: 

Dahili veya özel bulut, yalnızca tek bir kuruluş tarafından kullanılan bir tür bulut bilişim ortamıdır. Bu bulut türü genellikle kuruluşun şirket içi altyapısına dağıtılır ve kuruluşun verilerini, uygulamalarını ve diğer işletme varlıklarını barındırmak ve yönetmek için kullanılır.

Dahili bulutlar tipik olarak, tek bir fiziksel sunucuda veya sunucu kümesinde birden fazla sanal sunucunun ve diğer kaynakların oluşturulmasına ve yönetilmesine izin veren sanallaştırma teknolojisi kullanılarak dağıtılır. Bu, kuruluşların ek donanım satın almaya ve bakımını yapmaya gerek kalmadan bulut kaynaklarını gerektiği gibi kolayca yukarı veya aşağı ölçeklendirmelerini sağlar.

-------------

- Private Cloud Project: 

Özel bulut projesi, bir kuruluş için özel bir bulut bilişim ortamının uygulanmasını içeren bir projedir. Özel bulut, yalnızca tek bir kuruluş tarafından kullanılan bir tür bulut bilişim ortamıdır. 

-------------------

- Private Cloud Security: 

Özel bulut güvenliği, özel bir bulut bilişim ortamını tehditlerden ve güvenlik açıklarından korumak için kullanılan önlemleri ve uygulamaları ifade eder. 

Özel bulut güvenliği genellikle aşağıdakiler de dahil olmak üzere bir dizi farklı önlem ve uygulamayı içerir:

Fiziksel güvenlik: Özel bulut için kullanılan sunucular ve diğer donanımlar da dahil olmak üzere kuruluşun şirket içi altyapısının güvenli ve yetkisiz erişime karşı korunmasını sağlamak.

Ağ güvenliği: Kuruluşun ağını ve verilerini dış tehditlere ve güvenlik açıklarına karşı korumak için güvenlik önlemleri ve denetimleri uygulamak.

Erişim kontrolü: Yalnızca yetkili kullanıcıların ve uygulamaların özel buluta erişebilmesini ve erişimin uygun şekilde yönetilmesini ve izlenmesini sağlamak için önlemler ve kontroller uygulamak.

Şifreleme: Yetkisiz kişilerce erişilememesini sağlamak için, aktarılmakta olan ve dinlenmekte olan verileri korumak için şifrelemeyi kullanma.

Olağanüstü durum kurtarma ve iş sürekliliği: Bir felaket veya başka bir aksaklık durumunda özel bulutun hızlı ve etkin bir şekilde geri yüklenebilmesini sağlamak için planlar ve prosedürler geliştirmek ve uygulamak.


---------------

- Private Cloud Storage: 

Özel bulut depolama, bir kuruluşun verilerini depolamak ve yönetmek için özel bir bulut bilişim ortamının kullanılmasını ifade eder.

----------

- Public Cloud Storage:

Genel bulut depolama, bir kuruluşun verilerini depolamak ve yönetmek için genel bulut bilişim ortamının kullanılmasını ifade eder. Genel bulut, bir üçüncü taraf sağlayıcının sahip olduğu ve işlettiği ve birden çok kuruluş tarafından verilerini, uygulamalarını ve diğer işletme varlıklarını barındırmak ve yönetmek için kullanılan bir tür bulut bilişim ortamıdır.

--------------

- Vertical Cloud Computing:

Dikey bulut bilişim veya dikey bulut, belirli uygulama kullanımı için bulut bilişim ve bulut hizmetlerinin optimizasyonunu tanımlamak için kullanılır.

------------

- PlusClouds

---------

- AWS EC2:

Amazon Elastic Compute Cloud (EC2), Amazon Web Services (AWS) tarafından sunulan bir bulut bilişim hizmetidir. Bulutta yeniden boyutlandırılabilir bilgi işlem kapasitesi sağlayarak kuruluşların bilgi işlem kaynaklarını gerektiği gibi hızlı ve kolay bir şekilde yukarı veya aşağı ölçeklendirmelerine olanak tanır.

EC2, kuruluşların bulutta bulut sunucuları olarak bilinen sanal sunucuları başlatmasına olanak tanır. Bu örnekler, kuruluşun özel ihtiyaçlarını karşılamak için farklı donanım türleri, işletim sistemleri ve diğer seçeneklerle yapılandırılabilir.

EC2, iyileştirilmiş ölçeklenebilirlik ve esneklik, şirket içi altyapıya kıyasla daha düşük maliyetler ve internet bağlantılı herhangi bir cihazı kullanarak kuruluşun bilgi işlem kaynaklarına her yerden kolayca erişme ve yönetme yeteneği dahil olmak üzere bir dizi avantaj sağlar.

-------------

- IBM Cloud: 

IBM Cloud, IBM tarafından sunulan bir bulut bilişim platformudur. Hizmet olarak altyapı (IaaS), hizmet olarak platform (PaaS) ve hizmet olarak yazılım (SaaS) dahil olmak üzere bir dizi bulut bilişim hizmeti sunar.

IBM Cloud, kuruluşların uygulamalarını, verilerini ve diğer işletme varlıklarını bulutta kolayca ve hızlı bir şekilde dağıtmasına ve yönetmesine olanak tanır. Uygulamaları geliştirmek, dağıtmak ve yönetmenin yanı sıra verileri depolamak ve yönetmek için bir dizi araç ve hizmet sunar.

IBM Cloud, artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve şirket içi altyapıya kıyasla daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak bulut kaynaklarına her yerden kolayca erişmelerini ve yönetmelerini sağlar.

-----------------

- Google Cloud:

Google Cloud, Google tarafından sunulan bir bulut bilişim platformudur. Hizmet olarak altyapı (IaaS), hizmet olarak platform (PaaS) ve hizmet olarak yazılım (SaaS) dahil olmak üzere bir dizi bulut bilişim hizmeti sunar.

Google Cloud, kuruluşların uygulamalarını, verilerini ve diğer işletme varlıklarını bulutta kolayca ve hızlı bir şekilde dağıtmasına ve yönetmesine olanak tanır. Uygulamaları geliştirmek, dağıtmak ve yönetmenin yanı sıra verileri depolamak ve yönetmek için bir dizi araç ve hizmet sunar.

Google Cloud, şirket içi altyapıya kıyasla artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve azaltılmış maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak bulut kaynaklarına her yerden kolayca erişmelerini ve yönetmelerini sağlar.


-----

- Red Hat Cloud:

Red Hat Cloud, Red Hat tarafından sunulan bir bulut bilişim platformudur. Hizmet olarak altyapı (IaaS), hizmet olarak platform (PaaS) ve hizmet olarak yazılım (SaaS) dahil olmak üzere bir dizi bulut bilişim hizmeti sunar.

Red Hat Cloud, kuruluşların uygulamalarını, verilerini ve diğer işletme varlıklarını bulutta kolayca ve hızlı bir şekilde dağıtmasına ve yönetmesine olanak tanır. Uygulamaları geliştirmek, dağıtmak ve yönetmenin yanı sıra verileri depolamak ve yönetmek için bir dizi araç ve hizmet sunar.

Red Hat Cloud, artan ölçeklenebilirlik ve esneklik, iyileştirilmiş performans ve güvenilirlik ve şirket içi altyapıya kıyasla daha düşük maliyetler dahil olmak üzere bir dizi avantaj sağlar. Ayrıca, kuruluşların internete bağlı herhangi bir cihazı kullanarak bulut kaynaklarına her yerden kolayca erişmelerini ve yönetmelerini sağlar.

----------------

# Linux temel kavramlar araştırması

### Terminalde kernal versiyonu nasıl öğrenilir?: uname -r

### Terminalde mevcut ip adresleri nasıl öğrenilir? ip a

### Terminalde boş disk alanı nasıl öğrenilir? df -h

### Terminalde bir servisin çalışıp çalışmadığı nasıl öğrenilir? systemctl status servis-ismi

### Terminalde bir klasörün kapladığı toplam boyutu(size) nasıl öğrenilir? du -sh dizin

### Terminalde açık olan portlar nasıl öğrenilir? netstat -tulpn / ss -tulpn

### Terminalde processlerin kullandığı CPU/RAM/Kullanıcı Bilgisi nasıl öğrenilir? top/htop



-------------------

# KURULUMLAR

- ## Apache: Apache bir http serveridir. Statik web sitesi çalıştırmayı, reverse proxy, load balancer veya http cache olarak kullanılmayı sağlar.

Yapılanlar: Apache kuruldu, içerisinde website çalıştırıldı. Aşağıdaki kavramlar araştırıldı, uygulandı.
	
1. VirtualHost: Bir makinenin içinde birden fazla web sunucusunu sanallaştırma olmadan 	bulundurmasına denir. Hangi domaine istek gelirse kullanıcıyı o websitesinin bulunduğu 		dizine yönlendirir (yapıldı)

Aşağıdaki komutlar ile virtualhost oluşturdum


```
vi /etc/apache2/sites-available/virtual.host.conf
```

```
# create new
# settings for new domain
<VirtualHost *:80>
    DocumentRoot /var/www/virtual.host
    ServerName www.virtual.host
    ServerAdmin webmaster@virtual.host
    ErrorLog /var/log/apache2/virtual.host.error.log
    CustomLog /var/log/apache2/virtual.host.access.log combined
</VirtualHost>
```

Bu komut ile oluşturduğum virtual hostu açtım ve apacheyi yeniden başlattım.

```
a2ensite virtual.host
systemctl reload apache2
```

Ardından bu virtual host için örnek bir site oluşturdum.

```
mkdir /var/www/virtual.host

vi /var/www/virtual.host/index.html
<html>
<body>
<div style="width: 100%; font-size: 40px; font-weight: bold; text-align: center;">
Virtual Host Test Page
</div>
</body>
</html>
```

2. CGI Scripts: Kullanıcı spesifik bir url için web sunucusuna istek attığında çalışan 		kodlardır. Web formlarında girdi alırken, veritabanı sorguları yapılırken vb kullanılırlar.

CGI modulünü aktifleştirdim.

```
root@www:~# a2enmod cgid
Enabling module cgid.
To activate the new configuration, you need to run:
  systemctl restart apache2

root@www:~# systemctl restart apache2
```

Test etmek için bir script oluşturdum.

```
# create a test script
root@www:~# cat > /usr/lib/cgi-bin/test_script <<'EOF'
#!/usr/bin/perl
print "Content-type: text/html\n\n";
print "Hello CGI\n";
EOF
root@www:~# chmod 705 /usr/lib/cgi-bin/test_script
# try to access
root@www:~# curl localhost/cgi-bin/test_script
Hello CGI
```

Bir tarayıcı sayfasından erişebilmek için CGI test sayfası oluşturdum.

```
root@www:~# vi /var/www/html/cgi-enabled/index.cgi
#!/usr/bin/python3

print("Content-type: text/html\n")
print("<html>\n<body>")
print("<div style=\"width: 100%; font-size: 40px; font-weight: bold; text-align: center;\">")
print("CGI Script Test Page")
print("</div>")
print("</body>\n</html>")

root@www:~# chmod 755 /var/www/html/cgi-enabled/index.cgi
```


3. Userdir aktifleştirildi: Kullanıcılar artık kendi ev dizinlerinde website oluşturabiliyorlar.

```
root@www:~# a2enmod userdir
Enabling module userdir.
To activate the new configuration, you need to run:
  systemctl restart apache2

root@www:~# systemctl restart apache2
```

```
ubuntu@www:~$ mkdir ~/public_html
ubuntu@www:~$ chmod 711 $HOME
ubuntu@www:~$ chmod 755 ~/public_html
ubuntu@www:~$ vi ~/public_html/index.html
<html>
<body>
<div style="width: 100%; font-size: 40px; font-weight: bold; text-align: center;">
UserDir Test Page
</div>
</body>
</html>
```

---------

- ## Nginx kurulumu: 

	 NGINX, aynı apache gibi açık kaynaklı bir web serving, reverse proxying, caching, load balancing, media streaming, ve daha fazlası için kullanılan bir yazılımdır. 

	1. VirtualHost kurulumu yapıldı

  Aşağıdaki komutlar ile virtualhost oluşturdum


```
vi /etc/nginx/sites-available/virtual.host.conf
```

```
# create new
server {
    listen       80;
    server_name  www.virtual.host;

    location / {
        root   /var/www/virtual.host;
        index  index.html index.htm;
    }
}
```

Bu komut ile oluşturduğum virtual hostu açtım ve nginxi yeniden başlattım.

```
mkdir /var/www/virtual.host
cd /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/virtual.host.conf ./
systemctl reload nginx
```

Ardından bu virtual host için örnek bir site oluşturdum.

```
vi /var/www/virtual.host/index.html
<html>
<body>
<div style="width: 100%; font-size: 40px; font-weight: bold; text-align: center;">
Virtual Host Test Page
</div>
</body>
</html>
```

	 2. CGI Scripts: Kullanıcı spesifik bir url için web sunucusuna istek attığında çalışan 		kodlardır. Web formlarında girdi alırken, veritabanı sorguları yapılırken vb kullanılırlar.

   ```
root@www:~# apt -y install fcgiwrap
root@www:~# vi /etc/nginx/fcgiwrap.conf
# create new
# for example, enable CGI under [/cgi-bin]
location /cgi-bin/ {
    gzip off;
    root  /var/www;
    fastcgi_pass  unix:/var/run/fcgiwrap.socket;
    include /etc/nginx/fastcgi_params;
    fastcgi_param SCRIPT_FILENAME  $document_root$fastcgi_script_name;
}

root@www:~# mkdir /var/www/cgi-bin
root@www:~# chmod 755 /var/www/cgi-bin
# add settings into [server] section of a site definition
root@www:~# vi /etc/nginx/sites-available/default
server {
        .....
        .....
        include fcgiwrap.conf;
}

root@www:~# systemctl enable fcgiwrap
root@www:~# systemctl reload nginx
```

```
root@www:~# vi /var/www/cgi-bin/index.cgi
#!/usr/bin/python3

print("Content-type: text/html\n")
print("<html>\n<body>")
print("<div style=\"width: 100%; font-size: 40px; font-weight: bold; text-align: center;\">")
print("CGI Script Test Page")
print("</div>")
print("</body>\n</html>")

root@www:~# chmod 705 /var/www/cgi-bin/index.cgi
```


	
	 3. Userdir aktifleştirildi: Kullanıcılar artık kendi ev dizinlerinde website oluşturabiliyorlar.

   ```

ubuntu@www:~$ chmod 711 $HOME
ubuntu@www:~$ mkdir ~/public_html
ubuntu@www:~$ chmod 755 ~/public_html
ubuntu@www:~$ vi ~/public_html/index.html
<html>
<body>
<div style="width: 100%; font-size: 40px; font-weight: bold; text-align: center;">
Nginx UserDir Test Page
</div>
</body>
</html>
```


-------------
- ## LAMP: 

  A “LAMP” stack is a group of open source software that is typically installed together in order to enable a server to host dynamic websites and web apps written in PHP. This term is an acronym which represents the Linux operating system, with the Apache web server. The site data is stored in a MySQL database, and dynamic content is processed by PHP.

Kuruldu. 10.0.76.18 olan makinada.



-------------

- ## LEMP: 

​		LAMP stacktaki apache yerine nginx var. Kuruldu. 10.0.96.94 olan makinada.

--------------------

- LAMP + Wordpress:

- LEMP + Wordpress

- LEMP + Drupal

- LAMP + Drupal

- Mysql Replication:

# Ubuntu 22.04 MySQL Master-Slave Replication Kurulumu

## MySQL kurulumlarını yapalım

Aşağıdaki komut ile iki sistemimize de mysql'i kuruyoruz.
```bash
sudo apt install mysql-server -y 
```

Kurulumumuzda master olacak sunucumuzda aşağıdaki komutu çalıştırarak replikasyonu yapacak sunucumuzun mysql sunucumuzun portuna erişimini açıyoruz.

```bash
sudo ufw allow from replica_server_ip to any port 3306
```

## Konfigürasyon aşamalarına geçelim

Master sunucumuzda aşağıdaki komutu çalıştırarak mysqld.cnf dosyasını açalım.

```bash
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

Bu dosyada belirteceğim kısımları eğer yorum satırı ise uncomment ederek belirteceğim şekilde düzenliyoruz.

```
bind-address            = master-sunuc-ip

server-id               = 1 (bu sayının konfigürasyonumuzdaki tüm sunucularda farklı olmaasına dikkat edin)

log_bin                       = /var/log/mysql/mysql-bin.log

binlog_do_db          = db (eğer birden fazla veri tabanını replike edecekseniz buna ek binlog_do_db satırı oluşturabilirsiniz. Örn db_1, db_2 gibi)
```

Bu konfigürasyonları yaptıktan sonra aşağıdaki komutu yazarak mysql servisimizi yeniden başlatıyoruz.

```bash
sudo systemctl restart mysql
```
## Replikasyon için master sunucumuzda bir kullanıcı oluşturalım

Aşağıdaki komut ile mysql arayüzünü açıyoruz.

```bash
sudo mysql
```

Aşağıdaki komutu kurulumumuzdaki slave olacak sunucumuzun ip adresini ekleyerek düzenliyoruz ve giriyoruz.

```sql
GRANT REPLICATION SLAVE ON *.* TO 'replica_user'@'replica_server_ip';
```

Ardından ise şu komutu çalıştırıyoruz.

```sql
FLUSH PRIVILEGES;
```

## Master sunucudan Binary Log Koordinatlarını Alma

MySQL, veritabanı olaylarını kaynağın ikili günlük dosyasından satır satır kopyalayarak ve her olayı kopya üzerinde uygulayarak çoğaltmayı uygular. MySQL'in ikili günlük dosyası konuma dayalı çoğaltmasını kullanırken, kopyaya, kaynağın ikili günlük dosyasının adını ve bu dosya içindeki belirli bir konumu ayrıntılandıran bir dizi koordinat sağlamalısınız. Eşleme daha sonra bu koordinatları, günlük dosyasında veritabanı olaylarını kopyalamaya başlaması gereken noktayı belirlemek ve halihazırda işlediği olayları izlemek için kullanır.

Önceki adımda açık bıraktığımız Master sunucumuzun MySQL konsoluna aşağıdaki komutları yazıyoruz.

```sql
FLUSH TABLES WITH READ LOCK;
SHOW MASTER STATUS;
```

Ekranda bunun gibi bir tablo çıktısı görmelisiniz.

```sql
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      899 | db           |                  |                   |
+------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

Bu tablodaki bilgiler işimize yarayacak.

## Slave sunucumuzun veritabanını master sunucumuz ile eşleyelim.

Master sunucumuzda aşağıdaki komutları kullanarak bir yedek alalım.

```bash
sudo mysqldump -u root db > db.sql
```

bashArdından MySQL konsolumuza girip şu komutu çalıştıralım.

```sql
UNLOCK TABLES;
exit
```

Aşağıdaki komutla aldığımız yedeği slave sunucumuza gönderelim.

```bash
scp db.sql slave-sunucu-kullanıcı-adi@replica_server_ip:/tmp/
```

Ardından slave sunucumuza geçelim ve MySQL konsoluna giriş yapalım.

```sql
CREATE DATABASE db;
exit
```

Son olarak bu komut ile yedeğimizi slave sunucumuzda kuralım.

```bash
sudo mysql db < /tmp/db.sql
```

## Slave sunucumuzun konfigurasyonlarını yapalım.

Master sunucumuzda aşağıdaki komutu çalıştırarak mysqld.cnf dosyasını açalım.

```bash
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

Bu dosyada belirteceğim kısımları eğer yorum satırı ise uncomment ederek belirteceğim şekilde düzenliyoruz.

```
bind-address            = slave-sunucu-ip

server-id               = 2 (bu sayının konfigürasyonumuzdaki tüm sunucularda farklı olmaasına dikkat edin)

log_bin                       = /var/log/mysql/mysql-bin.log

binlog_do_db          = db (eğer birden fazla veri tabanını replike edecekseniz buna ek binlog_do_db satırı oluşturabilirsiniz. Örn db_1, db_2 gibi)
```

Bu dosyanın sonuna master sunucumuzdan farklı olarak şu satırı ekleyelim.

```
relay-log               = /var/log/mysql/mysql-relay-bin.log
```

Son olarak MySQL servisimizi yeniden başlatalım.

```bash
sudo systemctl restart mysql
```

## Replikasyonu başlatalım

Slave sunucumuzda MySQL konsolumuzu açalım ve aşağıdaki komutları girelim.

```
CHANGE REPLICATION SOURCE TO
SOURCE_HOST='source_server_ip',
SOURCE_USER='replica_user',
SOURCE_PASSWORD='password',
SOURCE_LOG_FILE='mysql-bin.000001',
SOURCE_LOG_POS=899;
```

Burada SOURCE_LOG_FILE ve SOURCE_LOG_POS kısmını size unutmayın dediğim tablodaki değerler ile değiştirmeniz gerek.

Artık replikasyonu başlatabiliriz.

```
START REPLICA;
```

Replikasyon işlemimizin detaylarına bakalım.

```
SHOW REPLICA STATUS\G;
```

Bize bunun gibi bir çıktı verecek.

```bash
*************************** 1. row ***************************
             Replica_IO_State: Waiting for master to send event
                  Source_Host: 138.197.3.190
                  Source_User: replica_user
                  Source_Port: 3306
                Connect_Retry: 60
              Source_Log_File: mysql-bin.000001
          Read_Source_Log_Pos: 1273
               Relay_Log_File: mysql-relay-bin.000003
                Relay_Log_Pos: 729
        Relay_Source_Log_File: mysql-bin.000001
```

 ## Replikasyonu test edelim

Master sunucumuza gelip MySQL konsolumuzu açalım. Oluşturduğumuz db veritabanına girelim.

```
USE db;
```

Veritabanında veri oluşturalım.

```
CREATE TABLE example_table (
example_column varchar(30)
);
```

Ardından slave sunucumuza gidelim ve MySQL konsolunu açalım.

```
USE db;
SHOW TABLES;
```

Bu iki komutu girdikten sonra bunun gibi bir çıktı almalısınız.

```
+---------------+
| Tables_in_db  |
+---------------+
| example_table |
+---------------+
1 row in set (0.00 sec)
```

Tebrikler kurulumu başarı ile tamamladınız.



----

- Apache Mem Cache

# UBUNTU 22.04 APACHE MEMCACHE KURULUMU

Memcached is an open-source object caching program that speeds up your database performance by caching data in memory. It is very useful for dynamic websites that allow repeated PHP object calls to be cached in system memory. It stores data based on key-values for small arbitrary strings or objects. It offers a lot of features including, ease of use, sub-millisecond latency, multithreaded architecture, data partitioning, support for multiple languages, and many more.

```
apt-get install memcached libmemcached-tools -y
```

Yukarıdaki komutu yazarak memcached yi sunucumuza kuruyoruz.

Ardından ise apacheyi sunucumuza indiriyoruz.

```
apt-get install apache2 php libapache2-mod-php php-memcached php-cli -y
```

Servislerimizi yeniden başlatıyoruz.

```
systemctl restart apache2
systemctl restart memcached
```

Ardından kurulumumuzu aşağıdaki gibi bir dosya oluşturarak doğruluyoruz.

```
nano /var/www/html/phpinfo.php

Add the following line:
<?php
phpinfo();
?>
```

Tarayıcımızdan bu sayfaya ulaşmaya çalıştığımızda şunun gibi bir sayfa çıkması gerek.

```
http://your-server-ip/phpinfo.php
```

![configure memcached on ubuntu 20.04](https://www.rosehosting.com/blog/wp-content/uploads/2021/01/configure-memcached-on-ubuntu-20.04.png)

----

- Apache Disk Cache

- Apache File Cache

- Varnish + Apache

- Varnish + Nginx

- Nginx WAF + OWASP

- Nginx Proxy

- Nginx LB

- Iptables: https://upcloud.com/resources/tutorials/configure-iptables-ubuntu

- FTP Server

- Cpanel

- Joomla

- NFS

- SMB

- GlusterFS

- ZFS

- Nginx CDN

- VPN

- Dynamic DNS

- Redis

- Crontab

- Backup Yöntemleri

- Logstash

- Kafka

- Elasticsearch

- RabbitMq

- Rsyslog

- PowerDNS

- HaProxy

- Disturbuted Wordpress

- Incremental Mysql Backup

- IIS + SQL + ASP

- CEPH

- iSCSI
