## Percona XtraDB Cluster kurulumumuzu yapalım

### Clusterde olacak makinelerimizin tümüne aşağıdaki kodları sırasıyla girerek gerekli indirmeleri yapalım.

```
sudo apt update
sudo apt install -y wget gnupg2 lsb-release curl
wget https://repo.percona.com/apt/percona-release_latest.generic_all.deb
sudo dpkg -i percona-release_latest.generic_all.deb
sudo apt update
sudo percona-release setup pxc80
sudo apt install -y percona-xtradb-cluster
```

### Clusterdeki makinelerimizin konfigürasyonlarını yapalım

Gerekli indirmeleri yaptıktan sonra tüm makinelerimizde mysql sunucularını konfigürasyon yapacağımız için durdurmamız gerekiyor.

~~~
sudo service mysql stop
~~~

**"/etc/mysql/mysql.conf.d/mysqld.cnf"** dosyasını tercih ettiğiniz bir editör ile açınız.

**"wsrep_cluster_address"** kısmına ağımızda bulunacak bilgisayarların ip adreslerini sırasıyla girelim. Ardından bu adımı clusterde bulunacak tüm cihazlar için tekrarlıyalım.

~~~
wsrep_cluster_address=gcomm://192.168.70.61,192.168.70.62,192.168.70.63
~~~

Birinci nodemizi konfigüre ediyoruz.

~~~
wsrep_node_name=pxc1
wsrep_node_address=192.168.70.61
pxc_strict_mode=ENFORCING
~~~

Diğer nodelerimizi de buna benzer şekilde fakat her [`wsrep_node_name`](https://docs.percona.com/percona-xtradb-cluster/8.0/wsrep-system-index.html#wsrep_node_name) and [`wsrep_node_address`](https://docs.percona.com/percona-xtradb-cluster/8.0/wsrep-system-index.html#wsrep_node_address)kısmı kendine has olacak şekilde düzenliyoruz.

Örneğin ikinci node için:

~~~
wsrep_node_name=pxc2
wsrep_node_address=192.168.70.62
~~~

Ben kurulumda ssl kullanmayacağım için bunu kapatacağım. Bunu yapmak için tüm makinelerimizde konfigürsayon dosyasına şu satırı ekleyelim.

~~~
pxc_encrypt_cluster_traffic = OFF
~~~

### Birinci nodemizi bootstrap moduna alalım

Bootstrap, bilinen herhangi bir cluster adresi olmadan ilk nodenin başlatılması anlamına gelir: wsrep_cluster_address değişkeni boşsa, Percona XtraDB Kümesi bunun ilk node olduğunu varsayar ve clusteri başlatır.

Bootstrap modunda başlatmak için aşağıdaki komutu ilk nodemizde çalıştırıyoruz.

~~~
systemctl start mysql@bootstrap.service
~~~

Önceki komutu kullanarak düğümü başlattığınızda, "**wsrep_cluster_address=gcomm://**" ile bootstrap modunda çalışır. Bu, nodeye clusteri "**wsrep_cluster_conf_id**" değişkeni 1'e ayarlı olarak başlatmasını söyler. Clustere başka node ekledikten sonra, bu nodeyi normal şekilde yeniden başlatabilirsiniz ve bu sayede standart ayarlarınızı tekrar kullanabilirsiniz.

Ardından ilk nodemizde mysql konsolunu açıp clusterin başlayıp başlamadığını kontrol edelim.

~~~
show status like 'wsrep%';
~~~

Şu şekilde bir çıktı vermesi gerek.

~~~
+----------------------------+--------------------------------------+
| Variable_name              | Value                                |
+----------------------------+--------------------------------------+
| wsrep_local_state_uuid     | c2883338-834d-11e2-0800-03c9c68e41ec |
| ...                        | ...                                  |
| wsrep_local_state          | 4                                    |
| wsrep_local_state_comment  | Synced                               |
| ...                        | ...                                  |
| wsrep_cluster_size         | 1                                    |
| wsrep_cluster_status       | Primary                              |
| wsrep_connected            | ON                                   |
| ...                        | ...                                  |
| wsrep_ready                | ON                                   |
+----------------------------+--------------------------------------+
40 rows in set (0.01 sec)
~~~



### İkinci nodemizi clustere ekleyelim

İkinci nodemizde mysql servisini çalıştıralım.

~~~
systemctl start mysql
~~~

Ardından bu nodemizin clustere bağlanıp bağlanmadığını kontrol edelim.

~~~
show status like 'wsrep%';
~~~

**wsrep_cluster_size**'nin 2 ye çıkması gerek.

~~~
+----------------------------------+--------------------------------------------------+
| Variable_name                    | Value                                            |
+----------------------------------+--------------------------------------------------+
| wsrep_local_state_uuid           | a08247c1-5807-11ea-b285-e3a50c8efb41             |
| ...                              | ...                                              |
| wsrep_local_state                | 4                                                |
| wsrep_local_state_comment        | Synced                                           |
| ...                              |                                                  |
| wsrep_cluster_size               | 2                                                |
| wsrep_cluster_status             | Primary                                          |
| wsrep_connected                  | ON                                               |
| ...                              | ...                                              |
| wsrep_provider_capabilities      | :MULTI_MASTER:CERTIFICATION: ...                 |
| wsrep_provider_name              | Galera                                           |
| wsrep_provider_vendor            | Codership Oy <info@codership.com>                |
| wsrep_provider_version           | 4.3(r752664d)                                    |
| wsrep_ready                      | ON                                               |
| ...                              | ...                                              | 
+----------------------------------+--------------------------------------------------+
75 rows in set (0.00 sec)
~~~

Ekleyeceğiniz diğer nodeleri de ikinci nodemizde yaptığımız işlemleri tekrarlayarak yapabilirsiniz.

Tüm nodeleri clustere bağladıktan sonra bootstrap modunu kapatabiliriz.

~~~
systemctl stop mysql@bootstrap.service
~~~

Ardından mysql servisimizi normal bir şekilde başlatalım.

~~~
systemctl start mysql
~~~

Artık clusterimizdeki makineler multi master konfigürasyonunda çalışmaya başladılar.

## Clustere dışarıdaki serverimizi slave olarak bağlayalım

### Makinemize Percona Server kurulumunu yapalım

Aşağıdaki komutları sırasıyla makinemizin terminaline girerek gereki kurulumları yapalım.

~~~
sudo apt update
sudo apt install curl
curl -O https://repo.percona.com/apt/percona-release_latest.generic_all.deb
sudo apt install gnupg2 lsb-release ./percona-release_latest.generic_all.deb
sudo apt update
sudo percona-release setup ps80
sudo apt install percona-server-server
~~~

### Master sunucumuzun konfigürasyonlarını yapalım

Percona Serverimizi (slave) clusterde bağlayacağımız makinenin (master) 3306 portunu slave kurulumu için açmamız gerekiyor.

Master olacak sunucumuzda aşağıdaki komutu girelim

~~~
sudo ufw allow from replica_server_ip to any port 3306
~~~

Ardından master sunucuda **"/etc/mysql/mysql.conf.d/mysqld.cnf"** dosyasını seçtiğimiz text editörümüz ile açıyoruz ve aşağıdaki kodları bu dosyaya ekliyoruz.

~~~
bind-adress = master_server_ip
server-id = 1
log_bin = /var/log/mysql/mysql-bin.log
binlog_do_db = db 
gtid_mode = ON
enforce-gtid-consistency = ON
~~~

**"master_server_ip"** = Master olacak makinenizin ip adresi

**"server-id"** = Her sunucunun kendisine özgü olması gereken sunucu idsi

**"binlog_do_db"** = Replikasyonun olmasını istediğiniz veritabanının ismi.

Bu dosyayı yukarıdaki gibi düzenledikten sonra mysql servisimizi yeniden başlatıyoruz.

~~~
sudo systemctl restart mysql
~~~

### Replikasyon için bir kullanıcı oluşturalım

Master sunucumuzda mysql konsolunu açalım ve aşağıdaki komutu girelim.

~~~
CREATE USER 'replica_user'@'replica_server_ip' IDENTIFIED WITH mysql_native_password BY 'password';
~~~

**"replica_user"** = replikasyon yapacak kullanıcı

**"replica_server_ip"** = slave sunucunun ip adresi

**"password"** = replikasyon kullanıcının şifresi

Ardından bu replikasyon yapacak kullanıcıya yetki vermemiz gerekiyor.

~~~
GRANT REPLICATION SLAVE ON *.* TO 'replica_user'@'replica_server_ip';
~~~

Bunun ardından şu komutu çalıştıralım.

~~~
FLUSH PRIVILEGES;
~~~

## Slave sunucumuzun konfigurasyonlarını yapalım.

Master sunucumuzda aşağıdaki komutu çalıştırarak mysqld.cnf dosyasını açalım.

```shell
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

Bu dosyada belirteceğim kısımları eğer yorum satırı ise uncomment ederek belirteceğim şekilde düzenliyoruz.

```plaintext
bind-address            = slave-sunucu-ip

server-id               = 2 (bu sayının konfigürasyonumuzdaki tüm sunucularda farklı olmaasına dikkat edin)

log_bin                       = /var/log/mysql/mysql-bin.log

binlog_do_db          = db (eğer birden fazla veri tabanını replike edecekseniz buna ek binlog_do_db satırı oluşturabilirsiniz. Örn db_1, db_2 gibi)

gtid_mode=ON
enforce-gtid-consistency=ON
log-replica-updates=ON
skip-replica-start=ON
```

Bu dosyanın sonuna master sunucumuzdan farklı olarak şu satırı ekleyelim.

```plaintext
relay-log               = /var/log/mysql/mysql-relay-bin.log
```

Son olarak MySQL servisimizi yeniden başlatalım.

```shell
sudo systemctl restart mysql
```

## Replikasyonu başlatalım

Slave sunucumuzda MySQL konsolumuzu açalım ve aşağıdaki komutları girelim.

```plaintext
CHANGE REPLICATION SOURCE TO
SOURCE_HOST='source_server_ip',
SOURCE_USER='replica_user',
SOURCE_PASSWORD='password',
SOURCE_AUTO_POSITION=1;
```

Burada SOURCE_LOG_FILE ve SOURCE_LOG_POS kısmını size unutmayın dediğim tablodaki değerler ile değiştirmeniz gerek.

Artık replikasyonu başlatabiliriz.

```plaintext
START REPLICA;
```

Replikasyon işlemimizin detaylarına bakalım.

```plaintext
SHOW REPLICA STATUS\G;
```

Bize bunun gibi bir çıktı verecek.

```
*************************** 1. row ***************************
             Replica_IO_State: Waiting for source to send event
                  Source_Host: 192.168.122.38
                  Source_User: replication_user
                  Source_Port: 3306
                Connect_Retry: 60
              Source_Log_File: mysql-bin.000001
          Read_Source_Log_Pos: 1109
               Relay_Log_File: ubuntu3-relay-bin.000002
                Relay_Log_Pos: 1325
        Relay_Source_Log_File: mysql-bin.000001
           Replica_IO_Running: Yes
          Replica_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Source_Log_Pos: 1109
              Relay_Log_Space: 1537
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Source_SSL_Allowed: No
           Source_SSL_CA_File: 
           Source_SSL_CA_Path: 
              Source_SSL_Cert: 
            Source_SSL_Cipher: 
               Source_SSL_Key: 
        Seconds_Behind_Source: 0
Source_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Source_Server_Id: 1
                  Source_UUID: 732c6aba-9bbc-11ed-a9a9-525400644621
             Source_Info_File: mysql.slave_master_info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
    Replica_SQL_Running_State: Replica has read all relay log; waiting for more updates
           Source_Retry_Count: 86400
                  Source_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Source_SSL_Crl: 
           Source_SSL_Crlpath: 
           Retrieved_Gtid_Set: 09df97bf-9bbb-11ed-9703-6e6b4394799a:1-4
            Executed_Gtid_Set: 09df97bf-9bbb-11ed-9703-6e6b4394799a:1-4
                Auto_Position: 1
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Source_TLS_Version: 
       Source_public_key_path: 
        Get_Source_public_key: 0
            Network_Namespace: 
1 row in set (0.00 sec)

ERROR: 
No query specified

```
Master sunucumuzda aşağıdaki kodu çalıştırdığımızda bize "Retrieved_Gtid_Set: 09df97bf-9bbb-11ed-9703-6e6b4394799a:1-4" kısmındaki gtid kodu ile aşağıda verecek gtid kodu aynı oluyorsa makineler eşleşti demektir.
```
show master status;
```

```
+------------------+----------+--------------+------------------+------------------------------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set                        |
+------------------+----------+--------------+------------------+------------------------------------------+
| mysql-bin.000001 |     1593 |              |                  | 09df97bf-9bbb-11ed-9703-6e6b4394799a:1-6 |
+------------------+----------+--------------+------------------+------------------------------------------+
```



## Replikasyonu test edelim

Master sunucumuza gelip MySQL konsolumuzu açalım. Oluşturduğumuz db veritabanına girelim.

```plaintext
USE db;
```

Veritabanında veri oluşturalım.

```plaintext
CREATE TABLE example_table (
example_column varchar(30)
);
```

Ardından slave sunucumuza gidelim ve MySQL konsolunu açalım.

```plaintext
USE db;
SHOW TABLES;
```

Bu iki komutu girdikten sonra bunun gibi bir çıktı almalısınız.

```plaintext
+---------------+
| Tables_in_db  |
+---------------+
| example_table |
+---------------+
1 row in set (0.00 sec)
```

Tebrikler kurulumu başarı ile tamamladınız.
